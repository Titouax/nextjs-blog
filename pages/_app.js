import '../styles/global.css'
import {ThemeProvider} from "styled-components";
import { GlobalStyles } from "../components/GlobalStyles";
import { lightTheme, darkTheme } from "../components/Themes";
import styles from './app.module.css'
import React, {useState} from 'react';

export default function App({ Component, pageProps }) {
    const [theme, setTheme] = useState('light');
    const themeToggler = () => {
        theme === 'light' ? setTheme('dark') : setTheme('light')
    }

    return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
        <>
            <GlobalStyles/>
              <button className={theme === 'light' ? styles.buttonLight : styles.buttonDark } onClick={themeToggler}>
              {theme === 'light' ? '🌙' : '☀️' }
              </button>  
            <Component {...pageProps} />
        </>
    </ThemeProvider>
    )
}